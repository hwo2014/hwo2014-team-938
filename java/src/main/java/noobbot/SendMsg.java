/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

public abstract class SendMsg {

	public String toJson() {
		return Main.GSON.toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();

}
