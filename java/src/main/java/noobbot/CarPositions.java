/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

import java.util.List;

public class CarPositions {

	String msgType;
	List<CarPosition> data;
	String gameId;
	int gameTick;


	static class CarPosition {
		Id id;
		double angle;
		PiecePosition piecePosition;
	}


	static class Id {
		String name;
		String color;
	}


	static class PiecePosition {
		int pieceIndex;
		double inPieceDistance;
		Lane lane;
		int lap;
	}


	static class Lane {
		int startLaneIndex;
		int endLaneIndex;
	}
}
