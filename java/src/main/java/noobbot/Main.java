package noobbot;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import noobbot.CarPositions.CarPosition;
import noobbot.GameInit.Piece;

public class Main {

	public static final Gson GSON = new Gson();

	private PrintWriter writer;
	private GameInit gameInit;
	private CarPositions latestCarPositions;

	private double throttle = 1.00;


	public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
		this.writer = writer;
		String line = null;

		send(join);

		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = GSON.fromJson(line, MsgWrapper.class);

			if (msgFromServer.msgType.equals("carPositions")) {
				latestCarPositions = GSON.fromJson(line, CarPositions.class);
				log();
				throttle = determineThrottle();
				//System.out.println("Throttle: " + throttle);
				send(new Throttle(throttle));

			} else if (msgFromServer.msgType.equals("crash")) {
				CarPosition self = latestCarPositions.data.get(0);
				System.out.println("!! Crashed: angle " + self.angle + "  |  pieceIdx: " + self.piecePosition.pieceIndex);
				System.out.println("Piece " + self.piecePosition.pieceIndex + " has angle of "
						+ gameInit.data.race.track.pieces.get(self.piecePosition.pieceIndex).angle
						+ " and radius of " + gameInit.data.race.track.pieces.get(self.piecePosition.pieceIndex).radius);
				System.exit(0);
			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("Joined");

			} else if (msgFromServer.msgType.equals("gameInit")) {
				gameInit = GSON.fromJson(line, GameInit.class);
				System.out.println("Race init");

			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("Race end");

			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("Race start");

			} else {
				send(new Ping());
			}
		}
	}

	private void log() {
		CarPosition self = latestCarPositions.data.get(0);
		System.out.println("LAP " + self.piecePosition.lap + ": \tangle " + self.angle + "\t\t |  pieceIdx: " + self.piecePosition.pieceIndex);
	}

	private double determineThrottle() {
		CarPosition self = latestCarPositions.data.get(0);

		final double MIN_THROTTLE = 0.42;
		double angle = Math.abs(self.angle);
		if (throttle > MIN_THROTTLE && angle > 50) {
			return throttle * 0.63;
		} else if (throttle > MIN_THROTTLE && angle > 40) {
			return throttle * 0.73;
		} else if (throttle > MIN_THROTTLE && angle > 30) {
			return throttle * 0.85;
		} else if (throttle > MIN_THROTTLE && angle > 20) {
			return throttle * 0.95;
		}

		if (throttle > 0.485 && angleIncoming(44, 4)) {
			return throttle * 0.504;
		}

		return Math.min(1.0, throttle * 2.825);
	}

	private boolean angleIncoming(double angle, int inNextNPieces) {
		CarPosition self = latestCarPositions.data.get(0);
		for (int i = self.piecePosition.pieceIndex; i < self.piecePosition.pieceIndex + inNextNPieces && i < gameInit.data.race.track.pieces.size(); ++i) {
			if (Math.abs(gameInit.data.race.track.pieces.get(i).angle) > angle) {
				//Piece piece = gameInit.data.race.track.pieces.get(i);
				//System.out.println("Angle inc after " + i + " pieces: angle " + piece.angle + " -- radius " + piece.radius);
				return true;
			}
		}
		return false;
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}

	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		socket.setTcpNoDelay(true);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new Main(reader, writer, new Join(botName, botKey));
	}
}
