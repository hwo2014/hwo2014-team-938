/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class GameInit {

	String msgType;
	Data data;


	static class Data {
		Race race;
	}


	static class Race {
		Track track;
		List<Car> cars;
		RaceSession raceSession;
	}


	static class Track {
		String id;
		String name;
		List<Piece> pieces;
		List<Lane> lanes;
	}


	static class Piece {
		double length;
		double radius;
		double angle;

		@SerializedName("switch")
		Boolean isSwitch;
	}


	static class Lane {
		double distanceFromCenter;
		int index;
	}


	static class Car {
		Id id;
		Dimensions dimensions;

		static class Id {
			String name;
			String color;
		}

		static class Dimensions {
			double length;
			double width;
			double guideFlagPosition;
		}
	}


	static class RaceSession {
		int laps;
		int maxLapTimeMs;
		boolean quickRace;
	}


}

